from struct import pack, unpack

from conversion import *

broadcast_mac = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff]
unknown_mac   = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00]

class Packet():
    @classmethod
    def analyze(cls, packet):
        #input: packet, a bytes object
        #output: cls object, a disassembled packet in its object form.
        raise NotImplementedError("analyze must be implemented by its subclasses")

    def get_packet(self):
        #returns a bytes object, a packet to be transmitted.
        raise NotImplementedError("get_packet must be implemented by its subclasses")

class ETH_packet(Packet):
    '''
    !   : Network byte order
    6B  : Destination MAC
    6B  : Source MAC
    H   : Ethertype(ETY)
    '''
    ETHER_FRAME='!6B6BH'

    @classmethod
    def analyze(cls, packet):
        eth_header=unpack(cls.ETHER_FRAME, packet[:14])
        self=ETH_packet()

        self.dst_mac=[*(eth_header[0:6])]
        self.src_mac=[*(eth_header[6:12])]

        self.ethertype=eth_header[12]

        self.payload=packet[14:]

        return self

    def __str__(self):
        return 'src MAC: {}\ndst MAC: {}\nethtype: {:04X}\n===PAYLOAD===\n{}'.format(arr_to_mac(self.src_mac), arr_to_mac(self.dst_mac), self.ethertype, self.payload)

    def get_packet(self):
        if isinstance(self.payload, Packet):
            return pack(self.ETHER_FRAME, *self.dst_mac, *self.src_mac, self.ethertype)+self.payload.get_packet()
        return pack(self.ETHER_FRAME, *self.dst_mac, *self.src_mac, self.ethertype)+self.payload

class ARP_packet(Packet):
    '''
    !   : Network byte order
    H   : Hardware Type(HRD)
    H   : Protocol Type(PRO)
    B   : Hardware Addr len(HLN)
    B   : Protocol Addr len(PLN)
    H   : Opcode(1 for request, 2 for response)
    6B  : Sender MAC addr
    4B  : Sender IP addr
    6B  : Target MAC addr
    4B  : Target IP addr
    '''
    ARP_FRAME='!HHBBH6B4B6B4B'
    HRD_BASE=0x0001
    PRO_BASE=0x0800
    HLN_BASE=0x06
    PLN_BASE=0x04

    ETHERTYPE=0x0806

    ARP_REQUEST=0x0001
    ARP_REPLY=0x0002

    @classmethod
    def analyze(cls, packet):
        if isinstance(packet, Packet):
            return packet

        arp_packet=unpack(cls.ARP_FRAME, packet[:28])
        self=ARP_packet()
        self.HRD=arp_packet[0]
        self.PRO=arp_packet[1]
        self.HLN=arp_packet[2]
        self.PLN=arp_packet[3]
        self.opcode=arp_packet[4]
        self.sender_mac=arp_packet[5:11]
        self.sender_ip=arp_packet[11:15]
        self.target_mac=arp_packet[15:21]
        self.target_ip=arp_packet[21:25]
        return self

    def __str__(self):
        return 'opcode : {}\nsnd MAC: {}\nsnd IP : {}\ntgt MAC: {} \ntgt IP : {}'.format(self.opcode, arr_to_mac(self.sender_mac), arr_to_ip(self.sender_ip), arr_to_mac(self.target_mac), arr_to_ip(self.target_ip))

    def get_packet(self, *, pad_len=18):
        return pack(self.ARP_FRAME, self.HRD, self.PRO, self.HLN, self.PLN, self.opcode,
                    *self.sender_mac, *self.sender_ip, *self.target_mac, *self.target_ip)+b'\x00'*pad_len#zero padding

def generate_arp_packet(opcode, src_mac, src_ip, dst_mac, dst_ip):
    pkt=ETH_packet()

    #ethernet header part
    pkt.src_mac            = src_mac
    pkt.dst_mac            = dst_mac if dst_mac!=unknown_mac else broadcast_mac
    pkt.ethertype          = ARP_packet.ETHERTYPE

    #arp packet part
    pkt.payload            = ARP_packet()
    pkt.payload.HRD        = ARP_packet.HRD_BASE
    pkt.payload.PRO        = ARP_packet.PRO_BASE
    pkt.payload.HLN        = ARP_packet.HLN_BASE
    pkt.payload.PLN        = ARP_packet.PLN_BASE
    pkt.payload.opcode     = opcode
    pkt.payload.sender_mac = src_mac
    pkt.payload.sender_ip  = src_ip
    pkt.payload.target_mac = dst_mac if dst_mac!=broadcast_mac else unknown_mac
    pkt.payload.target_ip  = dst_ip

    return pkt

if __name__ == '__main__':
    e=generate_arp_packet(
            opcode      = ARP_packet.ARP_REPLY,
            src_mac     = [3,3,3,3,3,3],
            src_ip      = [1,1,1,1],
            dst_mac     = [2,2,2,2,2,2],
            dst_ip      = [2,2,2,2],
            )
    e.payload=ARP_packet.analyze(e.payload)
    print(e)
