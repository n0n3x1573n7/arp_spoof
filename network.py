import socket, sys
from uuid import getnode as get_mac
from multiprocessing import Process, Array
from time import sleep

from packet import *
from conversion import *

debuglog=sys.stdout
logfile=sys.stdout

debug=lambda *args, **kwargs: print('\nDEBUG |', *args, **kwargs, file=debuglog)#None
logs =lambda *args, **kwargs: print('\nLOG   |', *args, **kwargs, file=logfile)#None

IP_TO_MAC_DICT={}

class Network():
    MAX_SIZE=65535
    ADD_BUFF=30#arbitrary value for extra buffer

    def __init__(self, interface):
        self.interface=interface

        self.reader=socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(0x0003))
        self.reader.bind((interface, 0))

        self.writer=socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(0x0003))
        self.writer.bind((interface, 0))

    def read_packet(self):
        #packet, addr
        return self.reader.recvfrom(self.MAX_SIZE+self.ADD_BUFF)

    def send_packet(self, packet):
        if isinstance(packet, Packet):
            self.writer.send(packet.get_packet())
        else:
            self.writer.send(packet)

    def close(self):
        self.reader.close()
        self.writer.close()

def get_mac_from_ip(ip, net, timeout=5):
    '''
    Get MAC address from IP address.
    '''
    global IP_TO_MAC_DICT
    
    if (*ip,) not in IP_TO_MAC_DICT:
        pkt=generate_arp_packet(
            opcode      = ARP_packet.ARP_REQUEST,
            src_mac     = get_local_mac(),
            src_ip      = get_local_ip(net.interface),
            dst_mac     = broadcast_mac,
            dst_ip      = ip,
            )

        mac=Array('i',6)

        def send_requests(mac):
            num=1
            mac[:]=[-1,-1,-1,-1,-1,-1]
            while mac[:]==[-1,-1,-1,-1,-1,-1]:
                debug('Trial {}'.format(num))
                num+=1
                net.send_packet(pkt)
                sleep(1)

        def recv_replies(mac):
            while True:
                pkt, addr=net.read_packet()
                eth=ETH_packet.analyze(pkt)
                if eth.ethertype==ARP_packet.ETHERTYPE:
                    arp=ARP_packet.analyze(eth.payload)
                    if arr_to_ip(arp.sender_ip)==arr_to_ip(ip):
                        mac[0:6]=arp.sender_mac[0:6]
                        return

        p1=Process(target=send_requests, args=(mac,))
        p2=Process(target=recv_replies, args=(mac,))

        p1.start()
        p2.start()

        try:
            p1.join(timeout)
            p2.join(timeout)

            IP_TO_MAC_DICT[(*ip,)]=mac[:]
            return IP_TO_MAC_DICT[(*ip,)]
        except:
            return

def get_local_ip(interface):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = ip_to_arr(s.getsockname()[0])
    s.close()
    return ip

def get_local_mac():
    '''
    Get local MAC address.
    '''
    mac=get_mac()
    arr=[]
    for _ in range(6):
        arr.append(mac&0xff)
        mac>>=8
    return [*reversed(arr)]

if __name__ == '__main__':
    net=Network('ens33')
    ip=[192,168,67,2]
    print(get_mac_from_ip(ip, net))